﻿using System;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SummationMas
{
    class PLINQSummation : ISummation
    {
        private readonly object locker = new object();
        private readonly int[] arrayInt;        
        private long sumArray;

        public PLINQSummation(int[] _arrayInt)
        {
            arrayInt = _arrayInt;
            sumArray = 0;
        }
        public void Sum()
        {
            Console.WriteLine("Вычисление суммы массива параллельное с помощью LINQ...");

            var stopWatch = new Stopwatch();
            stopWatch.Start();           
            
            arrayInt.AsParallel().ForAll(x=>AddSum(x));

            stopWatch.Stop();
            Console.WriteLine($"Сумма элементов массива {sumArray} подсчитана за {stopWatch.ElapsedMilliseconds} мс");               
        }

        private void AddSum(int item)
        {
            lock (locker)
            {
                sumArray += item;
                
            }
        }
    }
}
