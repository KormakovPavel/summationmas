﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SummationMas
{
    public static class GenerateArray
    {
        public static int[] Generate(int sizeArray)
        {            
            int[] arrayInt = new int[sizeArray];

            var random = new Random();
            for (int i = 0; i < sizeArray; i++)
            {
                arrayInt[i] = random.Next();                
            }

            return arrayInt;
        }
    }
}
