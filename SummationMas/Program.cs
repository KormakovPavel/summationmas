﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SummationMas
{
    class Program
    {
        static void Main()
        {
            Console.Write("Введите размер массива: ");
            int.TryParse(Console.ReadLine(),out int sizeArray);

            if(sizeArray!=0)
            {
                Console.WriteLine("Генерация массива...");
                var arrayInt = GenerateArray.Generate(sizeArray);
                Console.WriteLine("Генерация завершена");

                var listSummation = new List<ISummation>() { new SimplySummation(arrayInt), new ParallelSummation(arrayInt), new PLINQSummation(arrayInt) };
                foreach (var summation in listSummation)
                {
                    summation.Sum();
                }
            }
            else
            {
                Console.WriteLine("Ошибка. Размер массива должен быть больше нуля");
            }           

            Console.ReadLine();
        }
    }
}
