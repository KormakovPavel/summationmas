﻿using System;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SummationMas
{
    class ParallelSummation : ISummation
    {
        private readonly object locker = new object();
        private readonly int[] arrayInt;        
        private long sumArray;        

        public ParallelSummation(int[] _arrayInt)
        {
            arrayInt = _arrayInt;
            sumArray = 0;
        }
        public void Sum()
        {
            Console.WriteLine("Вычисление суммы массива параллельное...");

            var stopWatch = new Stopwatch();
            stopWatch.Start();

            var result = Parallel.ForEach(arrayInt, AddSum);

            if (result.IsCompleted)
            {
                stopWatch.Stop();
                Console.WriteLine($"Сумма элементов массива {sumArray} подсчитана за {stopWatch.ElapsedMilliseconds} мс");                
            }            
        }
        private void AddSum(int item)
        {
            lock(locker)
            {
                sumArray += item;
            }            
        }
    }
}
