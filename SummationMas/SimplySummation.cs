﻿using System;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;

namespace SummationMas
{
    class SimplySummation : ISummation
    {
        private readonly int[] arrayInt;        
        public SimplySummation(int[] _arrayInt)
        {
            arrayInt = _arrayInt;           
        }
        public void Sum()
        {
            Console.WriteLine("Вычисление суммы массива последовательное...");

            var stopWatch = new Stopwatch();
            stopWatch.Start();

            long sumArray=0;
            foreach (var item in arrayInt)
            {
                sumArray += item;
            }

            stopWatch.Stop();
            Console.WriteLine($"Сумма элементов массива {sumArray} подсчитана за {stopWatch.ElapsedMilliseconds} мс");
        }
    }
}
